#!/usr/bin/env python

from time import sleep
import os
import curses

screen = curses.initscr()
curses.noecho()
curses.curs_set(0)
screen.keypad(1)

screen.clear()

while True:
	event = screen.getch()
	if event == ord('q'):
		break
	if event == curses.KEY_UP:
		#screen.addstr(0, 0, "You pressed up!")
		os.system('aplay up.wav &')
	if event == curses.KEY_DOWN:
		#screen.clear()
		screen.addstr(0, 0, "You pressed down!")
		os.system('aplay down.wav &')
	if event == curses.KEY_LEFT:
		#screen.clear()
		screen.addstr(0, 0, "You pressed left!")
		os.system('aplay left.wav')
	if event == curses.KEY_RIGHT:
		#screen.clear()
		screen.addstr(0, 0, "You pressed right!")
		os.system('aplay right.wav')
		
        sleep(1);
