piboard
=======

Curses soundboard program for Raspberry PI
v0.1

Requirements
============
Python

$ sudo apt-get update
$ sudo apt-get install python-dev

Optional (GPIO for RPI)
$ sudo apt-get install python-rpi.gpio

Alsa audio drivers

$ sudo apt-get install alsa-utils

Optional mpg player
$ sudo apt-get install mpg321

Load the sound drivers and setup audio output for 3.5mm phono jack output
$ sudo modprobe snd-bcm2835
$ sudo amixer cset numid=3 1